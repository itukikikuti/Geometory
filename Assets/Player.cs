﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private Transform camera;

    private Vector3 cameraPosition;
    private Vector3 cameraAngles;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        cameraAngles = transform.eulerAngles;
    }

    void OnDestroy()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        Look();
        Move();
        Jump();
    }

    void Look()
    {
        cameraAngles += new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * 5f;
        cameraAngles.x = Mathf.Clamp(cameraAngles.x, -90f, 90f);

        camera.eulerAngles = cameraAngles;
    }

    void Move()
    {
        Vector3 forward = Vector3.Scale(camera.forward, new Vector3(1f, 0f, 1f)).normalized;
        Vector3 right = camera.right;
        Vector3 velocity = (forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal")) * moveSpeed;
        rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);
    }

    void Jump()
    {
        if (!Physics.CheckSphere(transform.position - new Vector3(0f, 0.7f, 0f), 0.4f))
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.y = jumpForce;
            rigidbody.velocity = velocity;
        }
    }
}
